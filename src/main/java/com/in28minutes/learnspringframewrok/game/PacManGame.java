package com.in28minutes.learnspringframewrok.game;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("pacManGame")
public class PacManGame implements GamingConsole {

    @Override
    public void up() {
        System.out.println("Go up");
    }

    @Override
    public void down() {
        System.out.println("Go down");
    }

    @Override
    public void left() {
        System.out.println("Go left");
    }

    @Override
    public void right() {
        System.out.println("Go right");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
