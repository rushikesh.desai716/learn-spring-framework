package com.in28minutes.learnspringframewrok.game;

public interface GamingConsole {

    void up();
    void down();
    void left();
    void right();
}
