package com.in28minutes.learnspringframewrok.springBasics;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

public class App02HelloWorldSpring {

    public static void main(String[] args) {

        // 1. Launch a Spring context.
        var context = new AnnotationConfigApplicationContext(HelloWorldConfiguration.class);

        // 2. Configure the things we want Spring to manage - @Configuration
        //      Use configuration class to launch the Spring context.

        //  Q. How to get beans from Spring Context?
        //  Ans. Ask the Spring context to provide the beans
        //      using "context.getBean()" method.

        //  Retrieving the Bean by Bean's method-name.
        System.out.println(context.getBean("FirstN"));
        System.out.println(context.getBean("age"));
        System.out.println("Retrieving the Bean by Bean's method-name: " + context.getBean("person"));

        //  Retrieving the Bean by Bean's user-defined name.
        System.out.println("Retrieving the Bean by Bean's user-defined name: " + context.getBean("MH Address"));

        //  Retrieving the Bean by class-name
        System.out.println("Retrieving the Bean by class-name: " + context.getBean(Address.class));

        System.out.println(context.getBean("Person Method Call"));
        System.out.println(context.getBean("Person Parameters"));
        System.out.println(context.getBean("Person Qualifier Parameters"));

        //  Retrieve all the Spring beans.
        Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);
        System.out.println("Number of beans: " + context.getBeanDefinitionCount());
    }
}
